package be.kriscoolen.opdracht1;

public class TextApp {
    public static void main(String[] args) {
        TextPrinter tp = new TextPrinter("Hello this is an example of a sentence containing words.");

        System.out.println("*** Words containing 'e' ***");
        tp.printFilteredWords(s->s.contains("e"));

        System.out.println("*** Long words ****");
        tp.printFilteredWords(s->s.length()>4);

        System.out.println("*** Words starting with 'a' ***");
        tp.printFilteredWords(s->s.startsWith("a"));

        System.out.println("*** Words with second letter equal to 'e' ***");
        tp.printFilteredWords(s-> {
            if (s.length() > 1) {
                return s.charAt(1) == 'e';
            }
            return false;
        });
        //Dit is veel korter en properder!
        //tp.printFilteredWords(s->s.length()>1&&s.charAt(1)=='e');

        System.out.println("*** Words with two times letter 'e' ***");
        tp.printFilteredWords(s->{
            int nE = 0;
            for(char c: s.toCharArray()){
                if(c=='e') nE++;
            }
            if(nE==2) return true;
            else return false;
        });

    }
}
