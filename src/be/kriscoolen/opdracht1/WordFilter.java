package be.kriscoolen.opdracht1;

@FunctionalInterface
public interface WordFilter {
    public boolean isValid(String s);
}
