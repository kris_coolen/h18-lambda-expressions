package be.kriscoolen.opdracht3;


import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class App {

    public static void main(String[] args) {
        Consumer<String> printer = System.out::print;
        printer.accept("*** Dit is een zin waar we alle woorden uithalen die een letter e bevatten***\n");

        TextPrinter tp1 = new TextPrinter("Dit is een zin waar we alle woorden uithalen die een letter e bevatten",printer);
        tp1.printFilteredWords(s->s.contains("e"));



        printer.accept("*** Dit is een zin die we gaan reversen: ***\n");
        TextPrinter tp2 = new TextPrinter("Dit is een zin die we gaan reversen!",printer);
        tp2.printProcessedWords(s->new StringBuilder(s).reverse().toString());

        System.out.println("*** Dit is een zin die we gaan scramblen. Cool!: ***");
        TextPrinter tp3 = new TextPrinter("Dit is een zin die we gaan scramblen. Cool!",printer);
        TextScrambler scrambler = new TextScrambler();
        tp3.printProcessedWords(scrambler::scramble);

        printer.accept("*** DiT IS EEN zIn DiE WE GAan OMZEtten nAAR KLeiNE LETTErs:\n");
        TextPrinter tp4 = new TextPrinter("DiT IS EEN zIn DiE WE GAan OMZEtten nAAR KLeiNE LETTErs",printer);
        tp4.printProcessedWords(String::toLowerCase);

        printer.accept("*** Dit is een zin waar we alle woorden uithalen die een letter e en een letter a bevatten: ***\n");
        TextPrinter tp5 = new TextPrinter("Dit is een zin waar we alle woorden uithalen die een letter e en een letter a bevatten.",printer);
        Predicate<String> cond1 = s->s.contains("a");
        Predicate<String> cond2 = s->s.contains("e");
        Predicate<String> cond3 = cond1.and(cond2);
        tp5.printFilteredWords(cond3);
        printer.accept("*** dit is een zin die we eerst gaan omzetten naar hoofdletters en vervolgens gaan omdraaien ***\n" );
        TextPrinter tp6 = new TextPrinter("dit is een zin die we eerst gaan omzetten naar hoofdletters en vervolgens gaan omdraaien",printer);
        Function<String,String> proc1 = String::toUpperCase;
        Function<String,String> proc2 = s->new StringBuilder(s).reverse().toString();
        Function<String,String> proc3 = proc1.andThen(proc2);
        tp6.printProcessedWords(proc3);



//        System.out.println("*** We gaan het volgende grote getal mooi afdrukken: 145 236 9852 3658");
//        TextPrinter tp4 = new TextPrinter("145 236 9852 3658");
//        tp4.printNumberValues(BigDecimal::new);
//
//        System.out.println("*** Dit is 1 zin met 543 getallen 8873 verstopt, waarvan we 12 de som 888 gaan afdrukken: 1");
//        TextPrinter tp5 = new TextPrinter("Dit is 1 zin met 543 getallen 8873 verstopt, waarvan we 12 de som 888 gaan afdrukken: 1");
//        tp5.printSum(BigDecimal::new);
    }
}
