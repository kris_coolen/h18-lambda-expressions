package be.kriscoolen.opdracht3;


import java.math.BigDecimal;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Consumer;

public class TextPrinter {
    private String sentence;
    private Consumer<String> printer;

    public TextPrinter(String sentence){
        this.sentence=sentence;
    }

    public TextPrinter(String sentence, Consumer<String> printer){
        this.sentence = sentence;
        this.printer = printer;

    }

    public void printFilteredWords(Predicate<String> filter){
        for(String w: sentence.split(" ")){
            if(filter.test(w)) printer.accept(w+" ");
        }
        printer.accept("\n");
    }

    public void printProcessedWords(Function<String,String> processor){
        for(String w: sentence.split(" ")){
            printer.accept(processor.apply(w) +" ");
        }
        printer.accept("\n");
    }

    public void printNumberValues(Function<String,BigDecimal> parser){
        for(String w: sentence.split(" ")){
           System.out.format("%,f%n", parser.apply(w));
       }
    }
//    public void printSum(NumberParser parser){
//        BigDecimal sum = new BigDecimal(0);
//        for(String w: sentence.split(" ")){
//            try{
//                sum=sum.add(parser.parse(w));
//                System.out.println(parser.parse(w));
//            }
//            catch(NumberFormatException e){
//
//            }
//
//        }
//        System.out.println("De som is gelijk aan: " + sum);
//
//    }


}
