package be.kriscoolen.opdracht3;

public class TextScrambler {

    public String scramble(String s){
        String newS;
        newS= s.replace('a','@');
        newS = newS.replace('e','€');
        newS=newS.replace('l','1');
        newS=newS.replace('o','0');
        return newS;
    }
}
