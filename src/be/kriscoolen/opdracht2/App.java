package be.kriscoolen.opdracht2;

import java.math.BigDecimal;

public class App {
    public static void main(String[] args) {

        System.out.println("*** Dit is een zin die we gaan reversen: ***");
        TextPrinter tp = new TextPrinter("Dit is een zin die we gaan reversen!");
        tp.printProcessedWords(TextUtil::reverse);

        System.out.println("*** Dit is een zin die we gaan scramblen. Cool!: ***");
        TextPrinter tp2 = new TextPrinter("Dit is een zin die we gaan scramblen. Cool!");
        TextScrambler scrambler = new TextScrambler();
        tp2.printProcessedWords(scrambler::scramble);

        System.out.println("*** DiT IS EEN zIn DiE WE GAan OMZEtten nAAR KLeiNE LETTErs:");
        TextPrinter tp3 = new TextPrinter("DiT IS EEN zIn DiE WE GAan OMZEtten nAAR KLeiNE LETTErs");
        tp3.printProcessedWords(String::toLowerCase);

        System.out.println("*** We gaan het volgende grote getal mooi afdrukken: 145 236 9852 3658 ***");
        TextPrinter tp4 = new TextPrinter("145 236 9852 3658");
        tp4.printNumberValues(BigDecimal::new);

        System.out.println("*** Dit is 1 zin met 543 getallen 8873 verstopt, waarvan we 12 de som 888 gaan afdrukken: 1");
        TextPrinter tp5 = new TextPrinter("Dit is 1 zin met 543 getallen 8873 verstopt, waarvan we 12 de som 888 gaan afdrukken: 1");
        tp5.printSum(BigDecimal::new);
    }
}
