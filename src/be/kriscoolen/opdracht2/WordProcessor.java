package be.kriscoolen.opdracht2;

@FunctionalInterface
public interface WordProcessor {
    public String process(String s);
}
