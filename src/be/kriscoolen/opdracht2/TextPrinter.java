package be.kriscoolen.opdracht2;

import be.kriscoolen.opdracht1.WordFilter;

import java.math.BigDecimal;

public class TextPrinter {
    private String sentence;

    public TextPrinter(String sentence){this.sentence=sentence;}

    public void printFilteredWords(WordFilter filter){
        for(String w: sentence.split(" ")){
            if(filter.isValid(w)) System.out.println(w);
        }
    }

    public void printProcessedWords(WordProcessor processor){
        for(String w: sentence.split(" ")){
            System.out.print(processor.process(w) +" ");
        }
        System.out.println("\n");
    }

    public void printNumberValues(NumberParser parser){
        for(String w: sentence.split(" ")){
            System.out.format("%,f%n", parser.parse(w));
        }
    }
    public void printSum(NumberParser parser){
        BigDecimal sum = new BigDecimal(0);
        for(String w: sentence.split(" ")){
            try{
                sum=sum.add(parser.parse(w));
                System.out.println(parser.parse(w));
            }
            catch(NumberFormatException e){
                System.out.println("'"+w + "' cannot be parsed to BigDecimal");

            }

        }
        System.out.println("De som is gelijk aan: " + sum);

    }

}
